use log::{debug, info};
use serde_derive::Deserialize;
use std::collections::HashMap;
use std::env;
use surf::Result;

pub async fn unsplash(query: String) -> Result<String> {
    #[derive(Deserialize, Debug)]
    struct URL {
        full: String,
    }

    #[derive(Deserialize, Debug)]
    struct Resp {
        urls: URL,
    }

    let api_key = env::var("UNSPLASH_KEY")?;
    let params: HashMap<_, _> = [
        ("query", query.as_str()),
        ("orientation", "landscape"),
        ("client_id", &api_key),
    ]
    .iter()
    .cloned()
    .collect();

    info!("Requesting Unsplash for images");
    let mut resp = surf::get("https://api.unsplash.com/photos/random")
        .query(&params)?
        .await?;
    debug!("unsplash response: {:?}", resp);

    let resp: Resp = resp.body_json().await?;

    Ok(resp.urls.full)
}
