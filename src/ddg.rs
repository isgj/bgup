use log::{debug, info};
use rand;
use regex::Regex;
use serde_derive::Deserialize;
use std::collections::HashMap;
use surf::{Error, Result};

pub async fn ddg(query: String) -> Result<String> {
    let token = get_ddg_token(&query).await?;

    #[derive(Deserialize, Debug)]
    struct Image {
        image: String,
    }

    #[derive(Deserialize, Debug)]
    struct Resp {
        results: Vec<Image>,
    }

    let params: HashMap<_, _> = [
        ("q", query.as_str()),
        ("l", "us-en"),
        ("o", "json"),
        ("vqd", token.as_str()),
        ("f", "size:Wallpaper,,layout:Wide,"),
        ("p", "1"),
    ]
    .iter()
    .cloned()
    .collect();

    info!("Request DuckDuckGo for images");
    let mut resp = surf::get("https://duckduckgo.com/i.js")
        .query(&params)?
        .await?;
    debug!("ddg json response: {:?}", resp);

    let resp: Resp = resp.body_json().await?;
    let index: usize = rand::random::<usize>() % resp.results.len();
    Ok(resp.results[index].image.to_owned())
}

async fn get_ddg_token(query: &String) -> Result<String> {
    let params: HashMap<_, _> = [
        ("q", query.as_str()),
        ("t", "canonical"),
        ("atb", "v175-1"),
        ("iax", "images"),
        ("ia", "images"),
        ("iaf", "size:Wallpaper,layout:Wide"),
    ]
    .iter()
    .cloned()
    .collect();

    info!("Request DuckDuckGo for the token");
    let mut resp = surf::get("https://duckduckgo.com/").query(&params)?.await?;
    debug!("ddg response: {:?}", resp);

    let re = Regex::new(r"vqd=(\d-\d+-\d+)")?;
    let body = resp.body_string().await?;
    for token in re.captures_iter(body.as_str()) {
        return Ok(token.get(1).unwrap().as_str().to_owned());
    }
    Err(Error::from_str(500, "vqd token not found"))
}
