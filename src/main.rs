use async_std;
use async_std::{fs, prelude::StreamExt};
use bgup;
use clap::{crate_authors, crate_version, Clap};
use dirs;
use env_logger;
use log::{debug, error, info};
use std::path::PathBuf;
use std::process::{self, Command};
use surf::Result;

/// Update the background with a random image
#[derive(Clap, Debug)]
#[clap(name = "BackgoundUpdate", version = crate_version!(), author = crate_authors!())]
struct Opts {
    /// choose the service from where to download the image
    #[clap(short, long, default_value = "ddg", possible_values = &["ddg", "pexel", "unsplash"])]
    service: String,
    /// search term to use in the query
    #[clap(short, long, default_value = "hd wallpaper")]
    query: String,
    /// the name to save the image on disk (defaults to the original name of the file)
    #[clap(short, long)]
    file_name: Option<String>,
    /// directory where the program will work (defaults to {PICTURE_DIR}/bgup)
    #[clap(short, long)]
    directory: Option<String>,
    #[clap(subcommand)]
    subcmd: Option<SubCommand>,
}

impl Opts {
    fn get_directory(&self) -> PathBuf {
        match &self.directory {
            Some(dir) => PathBuf::from(dir),
            None => dirs::picture_dir().unwrap().join("bgup"),
        }
    }

    async fn download_image(self) -> Result<()> {
        let exit_handler = |message| {
            move |err| {
                error!("{}: {}", message, err);
                process::exit(1);
            }
        };
        let img_url = match self.service.as_str() {
            "pexel" => bgup::pexel(self.query)
                .await
                .unwrap_or_else(exit_handler("Pexel service")),
            "unsplash" => bgup::unsplash(self.query)
                .await
                .unwrap_or_else(exit_handler("Unsplash service")),
            _ => bgup::ddg(self.query)
                .await
                .unwrap_or_else(exit_handler("DuckDuckGo service")),
        };

        let file_path = bgup::build_out_path(
            self.directory,
            self.file_name,
            img_url.split('/').last().unwrap_or("wallpaper.jpg"),
        )
        .await
        .unwrap();

        let file_path = bgup::download_from(img_url, file_path).await.unwrap();

        info!("Set the image as background");
        Command::new("gsettings")
            .arg("set")
            .arg("org.gnome.desktop.background")
            .arg("picture-uri")
            .arg(format!("file://{}", file_path.to_str().unwrap()))
            .output()
            .unwrap_or_else(|err| {
                error!("Setting background: {}", err);
                process::exit(1);
            });
        Ok(())
    }
}

#[derive(Clap, Debug)]
enum SubCommand {
    #[clap(version = crate_version!(), author = crate_authors!())]
    Clean(Clean),
}

/// clean the folder
#[derive(Clap, Debug)]
struct Clean {
    /// files less than (in KB) will be deleted
    #[clap(short, long, default_value = "4")]
    less_then: u64,
}

impl Clean {
    async fn run_in_path(self, path: PathBuf) -> Result<()> {
        let mut entries = fs::read_dir(path).await?;
        while let Some(file) = entries.next().await {
            let file = file?;
            let meta = file.metadata().await?;
            if meta.is_file() && meta.len() < (self.less_then * 1024) {
                debug!("Removing file: {}", file.file_name().to_str().unwrap());
                fs::remove_file(file.path()).await?;
            }
        }
        Ok(())
    }
}

fn main() {
    env_logger::init_from_env(env_logger::Env::default().default_filter_or("warn"));
    let mut opts: Opts = Opts::parse();

    async_std::task::block_on(async {
        match opts.subcmd.take() {
            Some(SubCommand::Clean(clean_cmd)) => {
                clean_cmd.run_in_path(opts.get_directory()).await.unwrap()
            }
            _ => opts.download_image().await.unwrap(),
        }
    });
}
