use log::{debug, info};
use serde_derive::Deserialize;
use std::collections::HashMap;
use std::env;
use surf::Result;

pub async fn pexel(query: String) -> Result<String> {
    #[derive(Deserialize, Debug)]
    struct URL {
        large2x: String,
    }

    #[derive(Deserialize, Debug)]
    struct Src {
        src: URL,
    }

    #[derive(Deserialize, Debug)]
    struct Resp {
        photos: Vec<Src>,
    }

    let params: HashMap<_, _> = [("query", query.as_str()), ("per_page", "1"), ("page", "1")]
        .iter()
        .cloned()
        .collect();

    info!("Requesting Pexel for images");
    let mut resp = surf::get("https://api.pexels.com/v1/search")
        .query(&params)?
        .header("Authorization", env::var("PEXEL_KEY")?)
        .await?;
    debug!("pexel response: {:?}", resp);

    let mut resp: Resp = resp.body_json().await?;

    Ok(resp.photos.remove(0).src.large2x)
}
