use async_std::fs;
use dirs;
use log::{debug, info, warn};
use std::path::PathBuf;
use surf::Result;

mod ddg;
mod pexel;
mod unsplash;
pub use ddg::ddg;
pub use pexel::pexel;
pub use unsplash::unsplash;

pub async fn download_from(uri: String, destination: PathBuf) -> Result<PathBuf> {
    debug!("Image uri: {}", uri);
    info!("Downloading the image");
    let bytes = surf::get(uri).recv_bytes().await?;
    debug!("Image size: {} bytes", bytes.len());
    if bytes.len() < 4096 {
        warn!(
            "The image might be corrupted, received only {} bytes",
            bytes.len()
        );
    }

    info!("Saving the image to the file");
    fs::write(&destination, bytes).await?;
    return Ok(destination);
}

pub async fn build_out_path(
    dir: Option<String>,
    name: Option<String>,
    original_name: &str,
) -> Result<PathBuf> {
    let dir = dir.map_or_else(|| dirs::picture_dir().unwrap().join("bgup"), PathBuf::from);

    if !dir.is_dir() {
        fs::create_dir_all(&dir).await?;
    }

    Ok(dir.join(name.unwrap_or(String::from(original_name))))
}
